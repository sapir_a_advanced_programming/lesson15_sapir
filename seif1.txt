.open magshimim.db
CREATE TABLE Students(studentID INT, studentName TXT, studentAge INT, studentGrade INT);
CREATE TABLE Classes(classID INT, className TXT, teacherID INT, catID INT);
CREATE TABLE Categories(catID INT, catName TXT);
CREATE TABLE Teachers(teacherID INT, teacherName TXT);
CREATE TABLE StudentsInClasses(studentID INT, classID INT);